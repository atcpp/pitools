/*
Copyright (c) 2019, Gianni Mariani
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this 
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this 
   list of conditions and the following disclaimer in the documentation and/or 
   other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may
   be used to endorse or promote products derived from this software without 
   specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef XX_VCGEN_COMMS__H_XX
#define XX_VCGEN_COMMS__H_XX

#include <string>
#include <sstream>
#include <vector>

#include <cstring>

extern "C" {
#include "interface/vmcs_host/vc_vchi_gencmd.h"
}

namespace vcgen {

/**
 * VcgenComms is a wrapper over the VCHI C communication functions provided
 * by "interface/vmcs_host/vc_vchi_gencmd.h" on Raspberry Pi.
 * 
 * Typical usage:
 *   vcgen::VcgenComms cmd;
 *   if (!cmd.send("measure_temp")) {
 *     std::cerr << cmd.getErrorStr() << std::endl;
 *   } else {
 *     std::cout << cmd.read() << std::endl;
 *   }
 *   if (!cmd.send("measure_clock arm")) {
 *     std::cerr << cmd.getErrorStr() << std::endl;
 *   } else {
 *     std::cout << cmd.read() << std::endl;
 *   }
 */
class VcgenComms {

public:
  VcgenComms() {
    ::vcos_init();
  }

  virtual ~VcgenComms() {
    disconnect();
    ::vcos_deinit();
  }

  bool connect() {
    if (vchi_initialise(&vchi_instance) != 0) {
      errorStr << "VCHI initialization failed";
      return false;
    }

    //create a vchi connection
    if (vchi_connect(NULL, 0, vchi_instance) != 0) {
      errorStr << "VCHI connection failed";
      return false;
    }

    connected = true;
    ::vc_vchi_gencmd_init(vchi_instance, &vchi_connection, 1);
  }

  bool disconnect() {
    if (!connected) {
      return true;
    }
    connected = false;

    ::vc_gencmd_stop();

    //close the vchi connection
    if (::vchi_disconnect(vchi_instance) != 0)
    {
      printf("VCHI disconnect failed\n");
      return -1;
    }
  }

  bool send(const std::string& command, int response_size = 1024) {

    response.clear();

    if (!connected) {
      if (!connect()) {
        return false;
      }
    }

    int ret;
    if ((ret = ::vc_gencmd_send("%s", command.c_str())) != 0) {
      errorStr << "vc_gencmd_send returned " << ret;
      return false;
    }

    response.resize(response_size);

    if ((ret = ::vc_gencmd_read_response(&(response[0]), response.size())) != 0) {
      errorStr << "vc_gencmd_read_response returned " << ret;
      return false;
    }

    response.resize(strlen(&(response[0])));

    return true;
  }

  std::string read() {
    return response;
  }

  const std::string getErrorStr() {
    return errorStr.str();
  }

private:

  bool connected{ false };
  std::ostringstream errorStr;
  std::string response;
  ::VCHI_INSTANCE_T vchi_instance{};
  ::VCHI_CONNECTION_T *vchi_connection{};
};


}

#endif
